#!/usr/bin/env bash

if ! docker start gitlab-homelab; then
  docker run --detach --publish 8080:8080 --publish 2222:22 \
    --name gitlab-homelab --env GITLAB_OMNIBUS_CONFIG="$GITLAB_OMNIBUS_CONFIG" \
    --volume $GITLAB_HOME/config:/etc/gitlab:Z  --volume $GITLAB_HOME/logs:/var/log/gitlab:Z  --volume $GITLAB_HOME/data:/var/opt/gitlab:Z \
    gitlab/gitlab-ee:latest
fi

while ! curl -s localhost:8080; do
  echo "We have some issues while attempting to connect to the homelab, trying again..."
  sleep 5
done

echo "Now access your GitLab server at $(gp url 8080)"
[ -f "$GITLAB_HOME/config/initial_root_password" ] && echo "Initial root password is $(sudo cat $GITLAB_HOME/config/initial_root_password)"