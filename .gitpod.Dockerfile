FROM gitpod/workspace-full

RUN brew update; \
    brew upgrade; \
    brew install direnv shellcheck hadolint; \
    sudo install-packages netcat

# Ensures direnv loader is added to bashrc.d
RUN direnv hook >> /home/gitpod/.bashrc.d/30-direnv